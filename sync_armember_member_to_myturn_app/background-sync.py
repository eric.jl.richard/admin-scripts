from sync_armember_member_to_myturn_app import config
from sync_armember_member_to_myturn import sync_armember_member_to_myturn
from armember_helper import ARMemberHelper
from myturn_helper import MyTurnHelper
import os
import re
import logging
import sys

logging.basicConfig(level=logging.INFO, stream=sys.stdout)

ah = None
mh = None

with os.scandir(config.SYNC_DIRECTORY) as it:
    for entry in it:
        if not entry.is_file():
            continue

        if not re.match(r"^[0-9]+$", entry.name):
            continue

        if ah is None:
            ah = ARMemberHelper(
                config.ARMEMBER_WORDPRESS_BASE_URL, config.ARMEMBER_SECURITY_KEY
            )

        if mh is None:
            mh = MyTurnHelper(config.MYTURN_USERNAME, config.MYTURN_PASSWORD)

        try:
            myturn_member = sync_armember_member_to_myturn(ah, mh, int(entry.name))
            logging.info("Sync'ed member id {}: {}".format(entry.name, myturn_member))
            os.remove(entry.path)
        except Exception as e:
            logging.error("Failed to sync member id {}".format(entry.name))
            logging.error(e)
