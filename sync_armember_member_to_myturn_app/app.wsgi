import os
from sync_armember_member_to_myturn_app import config

os.environ['ARMEMBER_WORDPRESS_BASE_URL'] = config.ARMEMBER_WORDPRESS_BASE_URL
os.environ['ARMEMBER_SECURITY_KEY'] = config.ARMEMBER_SECURITY_KEY
os.environ['MYTURN_USERNAME'] = config.MYTURN_USERNAME
os.environ['MYTURN_PASSWORD'] = config.MYTURN_PASSWORD
os.environ['SYNC_DIRECTORY'] = config.SYNC_DIRECTORY

from sync_armember_member_to_myturn_app import app as application
