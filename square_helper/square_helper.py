# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (c) 2020 Simon Marchi <simon.marchi@polymtl.ca>

from square.client import Client
import logging
import uuid
import os


class SquareApiError(Exception):
    def __init__(self, errors):
        self._errors = errors
        assert type(self._errors) is list

    def __str__(self):
        s = ""

        for error in self._errors:
            s += str(error)
            s += "\n"

        return s


class SquareHelper:
    def __init__(self, access_token):
        self._client = Client(access_token=access_token, environment="production")
        self._logger = logging.getLogger(__name__)

    def list_customers(self, progress=None):
        self._logger.info("list_customers")
        customers_api = self._client.customers
        return self._get_paginated(
            customers_api.list_customers, "customers", progress=progress
        )

    def get_customer(self, customer_id):
        self._logger.info(f"get_customer (customer_id='{customer_id}')")
        customers_api = self._client.customers
        result = customers_api.retrieve_customer(customer_id)

        if result.is_success():
            return result.body["customer"]
        else:
            raise SquareApiError(result.errors)

    def search_customers(self, email=None, progress=None):
        self._logger.info(f"search_customers (email='{email}')")
        customers_api = self._client.customers

        body = {"query": {"filter": {}}}

        if email is not None:
            body["query"]["filter"]["email_address"] = {"exact": email}

        return self._get_paginated(
            customers_api.search_customers, "customers", body=body, progress=progress
        )

    def list_payments(self, location_id=None, progress=None):
        self._logger.info("list_payments")
        payments_api = self._client.payments
        return self._get_paginated(
            payments_api.list_payments,
            "payments",
            progress=progress,
            location_id=location_id,
        )

    def list_locations(self):
        self._logger.info("list_locations")
        locations_api = self._client.locations
        result = locations_api.list_locations()

        if result.is_success():
            return result.body["locations"]
        else:
            raise SquareApiError(result.errors)

    def _get_paginated(self, api_method, body_key, body=None, progress=None, **kwargs):
        objects = []
        cursor = None

        while True:
            if body is not None:
                body["cursor"] = cursor
                result = api_method(body=body)
            else:
                result = api_method(cursor=cursor, **kwargs)

            if result.is_success():
                if body_key in result.body:
                    self._logger.debug(f"Received {len(result.body[body_key])} items.")
                    objects.extend(result.body[body_key])

                if "cursor" in result.body:
                    if progress is not None:
                        progress(len(objects))

                    cursor = result.body["cursor"]
                else:
                    return objects
            else:
                raise SquareApiError(result.errors)

    def get_orders(self, order_ids):
        assert type(order_ids) is list

        # Make sure there are no duplicates.
        order_ids = list(set(order_ids))

        self._logger.info(f"get_orders {order_ids}")
        locations_api = self._client.locations
        locations_result = locations_api.list_locations()

        if locations_result.is_error():
            raise SquareApiError(locations_result.errors)

        orders_api = self._client.orders
        orders = []

        while len(order_ids) > 0:
            order_ids_slice = order_ids[:100]
            order_ids = order_ids[100:]

            orders_result = orders_api.batch_retrieve_orders(
                {"order_ids": order_ids_slice}
            )

            if orders_result.is_success():
                if "orders" in orders_result.body:
                    orders.extend(orders_result.body["orders"])
            else:
                raise SquareApiError(orders_result.errors)

        return orders

    def create_customer(self, first_name, last_name, email):
        idempotency_key = str(uuid.uuid1())

        request_body = {
            "idempotency_key": idempotency_key,
            "given_name": first_name,
            "family_name": last_name,
            "email_address": email,
        }

        customers_api = self._client.customers
        result = customers_api.create_customer(request_body)

        if result.is_success():
            pass
        elif result.is_error():
            raise SquareApiError(result.errors)

    def get_inventory_counts(
        self, catalog_object_ids=None, location_ids=None, progress=None
    ):
        self._logger.info(f"get_inventory_counts {catalog_object_ids}. {location_ids}")
        inventory_api = self._client.inventory
        body = {}

        if catalog_object_ids is not None:
            body["catalog_object_ids"] = catalog_object_ids

        if location_ids is not None:
            body["location_ids"] = location_ids

        return self._get_paginated(
            inventory_api.batch_retrieve_inventory_counts,
            "counts",
            body=body,
            progress=progress,
        )

    def get_catalog_objects(self, object_ids, progress=None):
        self._logger.info(f"get_catalog_objects {object_ids}")
        catalog_api = self._client.catalog
        objects = []

        while len(object_ids) > 0:
            object_ids_slice = object_ids[:20]
            object_ids = object_ids[20:]

            objects_result = catalog_api.batch_retrieve_catalog_objects(
                {"object_ids": object_ids_slice}
            )

            if objects_result.is_success():
                if "objects" in objects_result.body:
                    objects.extend(objects_result.body["objects"])
            else:
                raise SquareApiError(objects_result.errors)

        return objects


def get_from_env():
    return SquareHelper(os.environ["SQUARE_ACCESS_TOKEN"])


if __name__ == "__main__":
    from pprint import pprint
    import sys

    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    sh = get_from_env()

    def progress(count):
        print(count)

    if sys.argv[1] == "list-customers":
        customers = sh.list_customers(progress=progress)

        print(f"{len(customers)} customers")

        pprint(customers)

    elif sys.argv[1] == "get-customer":
        customer = sh.get_customer(sys.argv[2])
        pprint(customer)

    elif sys.argv[1] == "list-payments":
        payments = sh.list_payments(progress=progress)

        print(f"{len(payments)} payments")

        pprint(payments)

    elif sys.argv[1] == "get-orders":
        order = sh.get_orders(sys.argv[2:])

        pprint(order)

    elif sys.argv[1] == "list-locations":
        locations = sh.list_locations()

        pprint(locations)
    elif sys.argv[1] == "search-customers":
        customers = sh.search_customers(progress=progress)

        pprint(customers)

    elif sys.argv[1] == "search-customers-email":
        customers = sh.search_customers(email=sys.argv[2], progress=progress)

        pprint(customers)

    elif sys.argv[1] == "get-inventory-counts":
        counts = sh.get_inventory_counts()
        pprint(counts)
