import logging
import os
import requests
import sys


class _HTTPError(Exception):
    def __init__(self, status, text):
        self._status = status
        self._text = text

    @property
    def status(self):
        return self._status

    @property
    def text(self):
        return self._text

    def __str__(self):
        return f"_HTTPError(status={self.status}, text={self.text})"


class APIError(Exception):
    def __init__(self, message):
        self._message = message

    @property
    def message(self):
        return self._message

    def __str__(self):
        return f"_APIError(message={self.message})"


class _Member:
    @staticmethod
    def from_json(json):
        m = _Member()

        m.display_name = json["display_name"]
        m.email = json["email"]
        m.first_name = json["first_name"]
        m.last_name = json["last_name"]
        m.username = json["username"]

        return m

    def __repr__(self):
        return f"_Member(display_name={self.display_name}, email={self.email}, first_name={self.first_name}, last_name={self.last_name})"


class ARMemberHelper:
    def __init__(self, base_url, security_key):
        self._base_url = base_url
        self._security_key = security_key
        self._logger = logging.getLogger(__name__)
        self._req = requests.Session()

    @staticmethod
    def _make_headers(headers=None):
        if headers is None:
            headers = {}

        headers[
            "User-Agent"
        ] = "Mozilla/5.0 (X11; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0"
        return headers

    def member_details(self, member_id):
        assert type(member_id) is int
        self._logger.debug(f"member_details (member_id={member_id})")
        url = f"{self._base_url}/wp-json/armember/v1/arm_member_details?arm_api_key={self._security_key}&arm_user_id={member_id}"
        resp = self._req.get(url, headers=self._make_headers())
        if resp.status_code != 200:
            raise _HTTPError(resp.status_code, resp.text)

        resp = resp.json()
        if resp["status"] != 1:
            raise APIError(resp["message"])

        self._logger.debug(f"response={str(resp)}")
        resp = resp["response"]
        resp = resp["result"]
        return _Member.from_json(resp)


def get_from_env():
    return ARMemberHelper(
        os.environ["ARMEMBER_WORDPRESS_BASE_URL"], os.environ["ARMEMBER_SECURITY_KEY"]
    )


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)
    helper = get_from_env()
    import pprint

    if sys.argv[1] == "member-details":
        member_id = int(sys.argv[2])
        details = helper.member_details(member_id)
        pprint.pprint(details)
