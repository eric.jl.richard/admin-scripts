# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (c) 2020 Simon Marchi <simon.marchi@polymtl.ca>

import square_helper
import creds
import logging
import sys

logging.basicConfig(level=logging.WARN, stream=sys.stdout)
logger = logging.getLogger(__name__)

sh = square_helper.SquareHelper(creds.square_access_token)

# Get all payments
payments = sh.list_payments()
order_ids = []

# Get order ids for all completed payments
for payment in payments:
    if payment["status"] != "COMPLETED":
        continue

    order_ids.append(payment["order_id"])

# Get all these orders
orders = sh.get_orders(order_ids)

part_sociale_catalog_object_id = "BWOM466KKIOPQ5WTFRJ76ITY"

customer_ids_who_bought_part_sociale = []


def get_order_customer_id(order):
    # There should ideally be a customer_id attached to the order. If so, use it.
    if "customer_id" in order:
        return order["customer_id"]

    # Else, we try to find one in one of the tenders.
    for tender in order["tenders"]:
        if "customer_id" in tender:
            return tender["customer_id"]

    return None


for order in orders:
    if order["state"] != "COMPLETED":
        # print("Not completed")
        continue

    if "line_items" not in order:
        # print("No items")
        continue

    has_part_sociale = False

    # Look for a "part sociale" item in the order's items.
    for item in order["line_items"]:
        if (
            "catalog_object_id" in item
            and item["catalog_object_id"] == part_sociale_catalog_object_id
        ):
            has_part_sociale = True
            break

    if has_part_sociale:
        customer_id = get_order_customer_id(order)
        if customer_id is not None:
            customer_ids_who_bought_part_sociale.append(customer_id)
        else:
            logging.warning(f"Order {order['id']} has part sociale but no customer id")

for customer_id in customer_ids_who_bought_part_sociale:
    customer = sh.get_customer(customer_id)

    given_name = customer["given_name"] if "given_name" in customer else None
    family_name = customer["family_name"] if "family_name" in customer else None
    email_address = customer["email_address"] if "email_address" in customer else None

    if given_name is None or family_name is None or email_address is None:
        logging.warning(
            f"Customer with id {order['id']} bought a part sociale, but we are missing some info:"
        )
        logging.warning(f"  missing given name: {given_name is None}")
        logging.warning(f"  missing family name: {family_name is None}")
        logging.warning(f"  missing email address: {email_address is None}")
        continue

    print(f" - {given_name} {family_name} - {email_address}")
