from flask import Flask, request, render_template
from inventaire_atelier_velo import get_inventaire

import square_helper


app = Flask(__name__)


@app.route("/")
def index():
    sh = square_helper.get_from_env()
    variations = get_inventaire(sh)
    show_prices = request.args.get("prix", default=False)
    variations.sort(key=lambda x: (x.item_name, x.var_name))
    total_value = sum([x.quantity * x.price for x in variations])
    return render_template(
        "index.html",
        variations=variations,
        show_prices=show_prices,
        total_value=total_value,
    )
